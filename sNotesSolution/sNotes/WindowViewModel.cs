﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;

namespace sNotes
{
    public class WindowViewModel
    {
        private MainWindow mainWindow;
        private int CornerRadiusValue = 5;
        private int ResizeBorderValue = 5;
        private int BorderThickness = 1;

        public Thickness GetResizeBorderThickness { get { return new Thickness(ResizeBorderValue); } }
        public CornerRadius GetCornerRadius { get { return new CornerRadius(CornerRadiusValue); } }
        public Thickness GetBorderThickness { get { return new Thickness(BorderThickness); } }

        public WindowViewModel(MainWindow mainWindow)
        {
            this.mainWindow = mainWindow;
        }
    }
}
