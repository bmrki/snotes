﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using sNotes.Classes;

namespace sNotes.Windows
{
    /// <summary>
    /// Interaction logic for NoteWindow.xaml
    /// </summary>
    public partial class NoteWindow : Window
    {
        private Note note;

        public NoteWindow()
        {
            InitializeComponent();
        }

        public NoteWindow(ref Note note)
        {
            InitializeComponent();

            this.note = note;
        }

        private void NW_title_TextChanged(object sender, TextChangedEventArgs e)
        {
            note.Title = (sender as TextBox).Text;
        }

        private void NW_color_Set()
        {
            string val = NW_color.Text;
            switch (val)
            {
                case "RED":
                    note.Color = Color.RED; break;
                case "ORANGE":
                    note.Color = Color.ORANGE; break;
                case "GREEN":
                    note.Color = Color.GREEN; break;
                case "BLUE":
                    note.Color = Color.BLUE; break;
                case "GRAY":
                    note.Color = Color.GRAY; break;
                default:
                    note.Color = Color.GRAY; break;
            }
        }

        private void NW_description_TextChanged(object sender, TextChangedEventArgs e)
        {
            note.Description = (sender as TextBox).Text;
        }

        private void NW_buttonSave_Click(object sender, RoutedEventArgs e)
        {
            NW_color_Set();
            MainWindow.isClosedBySaving = true;
            this.Close();
        }
    }
}
