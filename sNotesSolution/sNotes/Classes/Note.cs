﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace sNotes.Classes
{
    [Serializable]
    public class Note
    {
        [XmlElement("Title")]
        private string title;
        [XmlElement("Description")]
        private string description;
        [XmlElement("Date")]
        private string date;
        [XmlElement("Color")]
        private Color color;
        [XmlElement("Id")]
        private string id;

        public string Title { get => title; set => title = value; }
        public string Description { get => description; set => description = value; }
        public string Date { get => date; set => date = value; }
        public Color Color { get => color; set => color = value; }
        public string Id { get => id; set => id = value; }

        public Note(string title, string description, string date, Color color)
        {
            this.Title = title;
            this.Description = description;
            this.Date = date;
            this.Color = color;

            id = GenerateID();
        }

        public Note(Note note)
        {
            this.Title = note.Title;
            this.Description = note.Description;
            this.Date = note.Date;
            this.Color = note.Color;
            this.Id = note.Id;
        }

        public Note()
        {
            this.Title = "Title";
            this.Description = String.Empty;
            this.Date = DateTime.Now.ToString("dd.MM.yyyy");
            this.Color = Color.GRAY;

            id = GenerateID();
        }

        public string GenerateID()
        {
            var customGUID = Guid.NewGuid().ToByteArray();
            customGUID[3] |= 0xF0;

            string newGUID = new Guid(customGUID).ToString().Replace("-", String.Empty);

            return newGUID;
        }
    }
}
