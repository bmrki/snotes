﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace sNotes.Classes
{
    public class NoteGUI
    {
        private MainWindow mainWindow;

        private StackPanel stackPanel;
        private Label label;
        private CheckBox checkBox;
        private Button button;

        private TextBlock textBlock;
        private Style style;

        public NoteGUI(MainWindow mainWindow)
        {
            this.mainWindow = mainWindow;
        }

        public StackPanel GenerateNoteGUI(Note note)
        {
            stackPanel = new StackPanel
            {
                Orientation = Orientation.Horizontal,
            };
            stackPanel.MouseEnter += mainWindow.Note_MouseEnter;
            stackPanel.MouseLeave += mainWindow.Note_MouseLeave;
            stackPanel.MouseDown += mainWindow.Note_MouseDown;
            stackPanel.SetValue(FrameworkElement.NameProperty, note.Id);

            textBlock = new TextBlock
            {
                Text = note.Title
            };

            style = new Style();

            switch (note.Color)
            {
                case Color.RED: style = mainWindow.styles["labelRedNote"]; break;
                case Color.ORANGE: style = mainWindow.styles["labelOrangeNote"]; break;
                case Color.GREEN: style = mainWindow.styles["labelGreenNote"]; break;
                case Color.BLUE: style = mainWindow.styles["labelBlueNote"]; break;
                case Color.GRAY: style = mainWindow.styles["labelGrayNote"]; break;
                default: style = mainWindow.styles["labelGrayNote"]; break;
            }

            label = new Label
            {
                Content = textBlock,
                Style = style
            };

            checkBox = new CheckBox();
            checkBox.Checked += mainWindow.CheckBox_Checked;
            checkBox.Unchecked += mainWindow.CheckBox_Unchecked;
            checkBox.Visibility = Visibility.Hidden;

            button = new Button
            {
                Style = mainWindow.styles["buttonTrash"]
            };

            button.Click += mainWindow.ButtonTrashNote_Click;
            button.Visibility = Visibility.Hidden;

            stackPanel.Children.Add(label);
            stackPanel.Children.Add(checkBox);
            stackPanel.Children.Add(button);

            return stackPanel;
        }
    }
}
