﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using sNotes.Windows;
using sNotes.Classes;
using System.Xml.Serialization;
using System.IO;
using System.Xml;

namespace sNotes
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public Grid mainGrid;
        public UIElementCollection mainGridChildren;
        public GridLengthConverter gridLengthConverter;

        public List<Note> notes;

        public Note note;
        public NoteGUI noteGUI;

        public bool isMenuOpen = false;
        public int gridRowsCount;
        public int howManyNotes;

        public Dictionary<string, Style> styles;
        public Dictionary<string, Brush> brushes;

        public static bool isClosedBySaving = false;

        public string path = "O:\\OneDrive\\Dokumenty\\notes.xml";

        public MainWindow()
        {
            InitializeComponent();
            WindowViewModel windowViewModel = new WindowViewModel(this);
            this.DataContext = windowViewModel;

            LoadResources();

            gridLengthConverter = new GridLengthConverter();
            mainGrid = MainGrid as Grid;
            mainGridChildren = MainGrid.Children;
            gridRowsCount = mainGrid.RowDefinitions.Count;

            // On start we have row for Func buttons and row above to hold Func buttons on the bottom so there is gridRowsCount - 2
            howManyNotes = gridRowsCount - 2;

            HideMenu();
            HideNotesMember();

            notes = new List<Note>();
            noteGUI = new NoteGUI(this);
        }

        /* NOTES AND IDEAS
        
            -> ComboBox do przerobienia
            -> Dodać wyświetlanie daty notatki
            -> Segregowanie notatek po datach
            -> Automatyczna zmiana koloru notatki w zależności od daty jej upłynięcia
            -> Dodać alert na określony czas przed upłynięciem daty notatki
            -> Kolor czcionki w notatce
            -> Ikony Func idą do góry gdy nie ma żadnej notatki, powinny zostawać na dole
        */

        /// <summary>
        /// Ubezpieczyć od null
        /// </summary>
        private void LoadResources()
        {
            styles = new Dictionary<string, Style>();
            brushes = new Dictionary<string, Brush>();

            styles.Add("labelRedNote", this.FindResource("LabelNoteRed") as Style);
            styles.Add("labelOrangeNote", this.FindResource("LabelNoteOrange") as Style);
            styles.Add("labelGreenNote", this.FindResource("LabelNoteGreen") as Style);
            styles.Add("labelBlueNote", this.FindResource("LabelNoteBlue") as Style);
            styles.Add("labelGrayNote", this.FindResource("LabelNoteGray") as Style);

            styles.Add("buttonTrash", this.FindResource("TrashButton") as Style);

            brushes.Add("primaryGrayDark", this.FindResource("PrimaryGrayBrushDark") as Brush);
            brushes.Add("primaryGray", this.FindResource("PrimaryGrayBrush") as Brush);
            brushes.Add("white", this.FindResource("WhiteBrush") as Brush);
        }

        /// <summary>
        /// Zoptymalizować dodając kolekcje przycisków do ukrycia/odkrycia
        /// </summary>
        private void HideMenu()
        {
            ButtonExit.Visibility = Visibility.Hidden;
            ButtonSettings.Visibility = Visibility.Hidden;
            ButtonLoad.Visibility = Visibility.Hidden;
            //ButtonRemoveAll.Visibility = Visibility.Hidden;
        }

        private void ShowMenu()
        {
            ButtonExit.Visibility = Visibility.Visible;
            ButtonSettings.Visibility = Visibility.Visible;
            ButtonLoad.Visibility = Visibility.Visible;
            //ButtonRemoveAll.Visibility = Visibility.Visible;
        }

        private void HideNotesMember()
        {
            UIElementCollection subStackPanel;

            foreach (UIElement node in mainGridChildren)
            {
                if (node.GetType().Equals(typeof(StackPanel)))
                {
                    subStackPanel = (node as StackPanel).Children;
                    foreach (UIElement n in subStackPanel)
                    {
                        if (n.GetType().Equals(typeof(Button)))
                        {
                            (n as Button).Visibility = Visibility.Hidden;
                        }
                        else if (n.GetType().Equals(typeof(CheckBox)))
                        {
                            (n as CheckBox).Visibility = Visibility.Hidden;
                        }
                    }
                }
            }
        }

        private void AddNote(Note note)
        {
            if (!NoteExist(note))
            {
                StackPanel stackPanel = noteGUI.GenerateNoteGUI(note); //new NoteGUI(this).GenerateNoteGUI(note);

                // Prepare row for new Note
                PrepareNewRow();

                // Set up ColumnSpan for new Note, row will be set up later
                Grid.SetColumnSpan(stackPanel, 6);

                // StackPanel (new Note) ready - add to mainGridChildren
                mainGridChildren.Add(stackPanel);

                howManyNotes++;
                NormalizeRows();
            }
        }

        private bool NoteExist(Note note)
        {
            foreach (UIElement el in mainGridChildren)
            {
                if (el.GetType().Equals(typeof(StackPanel)))
                {
                    if (el.GetValue(FrameworkElement.NameProperty).Equals(note.Id))
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        private void PrepareNewRow()
        {
            if (howManyNotes == 0)
                mainGrid.RowDefinitions.RemoveAt(0);

            RowDefinition rowDefinition = new RowDefinition
            {
                Height = (GridLength)gridLengthConverter.ConvertFrom("*")
            };
            mainGrid.RowDefinitions.Insert(howManyNotes, rowDefinition);
        }

        private void NormalizeRows()
        {
            int i = 0;
            foreach (UIElement el in mainGrid.Children)
            {
                if (!el.GetType().Equals(typeof(Button)))
                {
                    Grid.SetRow(el, i);
                    i++;
                }
            }
            // Set up FUNC buttons on the last row
            Grid.SetRow(ButtonMenu, i);
            Grid.SetRow(ButtonSettings, i);
            Grid.SetRow(ButtonLoad, i);
            //Grid.SetRow(ButtonRemoveAll, i);
            Grid.SetRow(ButtonExit, i);
            Grid.SetRow(ButtonAdd, i);

            // Check for HEIGHT of a Window and adjust it
            int checkWindowHeight = i * 60;
            if (checkWindowHeight >= 300)
                this.Height = checkWindowHeight;
        }

        private Label GetLabel(object sender)
        {
            StackPanel stackPanel = GetParentAsStackPanel(sender);

            foreach (UIElement el in stackPanel.Children)
            {
                if (el.GetType().Equals(typeof(Label)))
                    return el as Label;
            }
            return null;
        }

        private StackPanel GetParentAsStackPanel(object sender)
        {
            DependencyObject parent = VisualTreeHelper.GetParent(sender as CheckBox);
            return (parent as StackPanel);
        }

        private void SerializeNotes(string filePath)
        {
            XmlSerializer xmlSerializer = new XmlSerializer(typeof(List<Note>));

            using (StreamWriter sw = new StreamWriter(filePath))
            {
                xmlSerializer.Serialize(sw, notes);
            }
        }

        private void DeserializeNotes(string filePath)
        {
            XmlSerializer xmlSerializer = new XmlSerializer(typeof(List<Note>));

            using (StreamReader sr = new StreamReader(filePath))
            {
                notes = (List<Note>)xmlSerializer.Deserialize(sr);
            }
        }

        private void ButtonExit_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void ButtonMenu_Click(object sender, RoutedEventArgs e)
        {
            if (!isMenuOpen)
            {
                ShowMenu();
                isMenuOpen = !isMenuOpen;
            }
            else
            {
                HideMenu();
                isMenuOpen = !isMenuOpen;
            }
        }

        public void Note_MouseEnter(object sender, MouseEventArgs e)
        {
            UIElementCollection subStackPanel = (sender as StackPanel).Children;

            foreach (UIElement n in subStackPanel)
                n.Visibility = Visibility.Visible;
        }

        public void Note_MouseLeave(object sender, MouseEventArgs e)
        {
            UIElementCollection subStackPanel = (sender as StackPanel).Children;

            foreach (UIElement n in subStackPanel)
                if (!(n.GetType().Equals(typeof(Label))))
                    n.Visibility = Visibility.Hidden;
        }

        /// <summary>
        /// Method for displaying NoteWindow
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void Note_MouseDown(object sender, MouseButtonEventArgs e)
        {

        }

        private void ButtonAdd_Click(object sender, RoutedEventArgs e)
        {
            note = new Note();
            NoteWindow noteWindow = new NoteWindow(ref note);
            noteWindow.Closed += OnNoteWindowClosing;
            noteWindow.Show();
        }

        private void OnNoteWindowClosing(object sender, EventArgs e)
        {
            if ((note != null) && (isClosedBySaving == true))
            {
                AddNote(note);
                notes.Add(note);
                SerializeNotes(path);
                isClosedBySaving = false;
            }
        }

        public void ButtonTrashNote_Click(object sender, RoutedEventArgs e)
        {
            if (howManyNotes > 0)
            {
                DependencyObject parent = VisualTreeHelper.GetParent(sender as Button);
                int noteRow = Grid.GetRow(parent as UIElement);
                string noteId = GetNoteId(parent as StackPanel);

                mainGridChildren.Remove(parent as UIElement);
                mainGrid.RowDefinitions.RemoveAt(noteRow);

                // Delete note from List and load all notes from List again to xml file
                Note n = notes.SingleOrDefault(i => i.Id.Equals(noteId));
                if (n != null)
                    notes.Remove(n);

                SerializeNotes(path);

                howManyNotes--;

                if (howManyNotes == 0)
                {
                    RowDefinition rowDefinition = new RowDefinition
                    {
                        Height = (GridLength)gridLengthConverter.ConvertFrom("*")
                    };
                    mainGrid.RowDefinitions.Insert(howManyNotes, rowDefinition);
                }
                else
                {
                    NormalizeRows();
                }
            }
        }

        private string GetNoteId(StackPanel stackPanel)
        {
            return (string)stackPanel.GetValue(FrameworkElement.NameProperty);
        }

        public void CheckBox_Checked(object sender, RoutedEventArgs e)
        {
            Label label = GetLabel(sender);
            if (label == null)
                return;

            TextBlock textBlock = label.Content as TextBlock;
            textBlock.TextDecorations = TextDecorations.Strikethrough;
            textBlock.Foreground = brushes["primaryGray"];

            label.Content = textBlock;
            label.Style = styles["labelGrayNote"];
        }

        public void CheckBox_Unchecked(object sender, RoutedEventArgs e)
        {
            Label label = GetLabel(sender);
            if (label == null)
                return;

            TextBlock textBlock = label.Content as TextBlock;
            textBlock.TextDecorations = null;
            textBlock.Foreground = brushes["white"];

            label.Content = textBlock;

            StackPanel stackPanel = GetParentAsStackPanel(sender);
            string noteID = GetNoteId(stackPanel);

            var n = notes.FirstOrDefault(o => o.Id.Equals(noteID));
            if (n == null)
            {
                label.Style = styles["labelGrayNote"];
            }
            else
            {
                switch (n.Color)
                {
                    case Color.RED: label.Style = styles["labelRedNote"]; break;
                    case Color.ORANGE: label.Style = styles["labelOrangeNote"]; break;
                    case Color.GREEN: label.Style = styles["labelGreenNote"]; break;
                    case Color.BLUE: label.Style = styles["labelBlueNote"]; break;
                    case Color.GRAY: label.Style = styles["labelGrayNote"]; break;
                    default: label.Style = styles["labelGrayNote"]; break;
                }
            }
        }

        private void ButtonLoad_Click(object sender, RoutedEventArgs e)
        {
            DeserializeNotes(path);

            if (notes != null)
            {
                foreach (Note n in notes)
                {
                    AddNote(n);
                }
            }
        }
    }
}
